class Solution(object):
    def __init__(self, string):
        self.string = string
        self.openingsymbols = ["[","{" ,"("]
        self.closingsymbols = ["]","}",")"]

        self.readsymbols = []
    
    def test(self):
        for elem in self.string:
            if elem == "[" or elem == "{" or elem == "(":
                self.readsymbols.append(elem)
            elif elem == "]" or elem == "}" or elem == ")":
                self.readsymbols.append(elem)
        
        matches = (x for x in self.readsymbols if x == self.openingsymbols[1])
        print(matches)

if __name__ == '__main__':
    solution=Solution("hola[mundo]")
    solution.test()